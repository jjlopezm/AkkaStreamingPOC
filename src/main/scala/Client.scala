import java.io.File

import akka.actor.{ActorRef, ActorSystem}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import akka.stream.io._

import scala.concurrent.Future

/**
  * Created by jjlopez on 25/04/16.
  */
object Client extends App{

  implicit val system = ActorSystem("Sys")
  implicit val materializer = ActorMaterializer()


  val file = new File("/home/jjlopez/Stratio/workspaceJDBC/JDBCPackage/CrossdataJDBC_v1.2.0-SNAPSHOT.tar.gz")

  val actorRef

  val sink=Sink.actorRef(actorRef,null)
  val foreach: Future[Long] = FileIO.fromFile(file)
    .to(sink)
    .run()


}
